import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {
  blockUser,
  getUserByUsername,
  unblockUser,
  updateUserRole,
} from '../../Services/user.services';
import { Dropdown, Button } from 'react-bootstrap';
import AppContext from '../../Providers/App.Context';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { getAddonsByAuthor } from '../../Services/addons.services';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import './UserProfile.css';

export default function UserProfile() {
  let { username } = useParams();
  const [user, setUser] = useState({});
  const [error, setError] = useState('');
  const [userAddons, setUserAddons] = useState([]);
  const [isBlocked, setIsBlocked] = useState(user.isBlocked);
  const { userData } = useContext(AppContext);

  useEffect(() => {
    setError('');
    getUserByUsername(username)
      .then((u) => {
        const userInfo = u.val();
        setUser(userInfo);
        return userInfo;
      })
      .then((u) => {
        return getAddonsByAuthor(u.userName).then((a) => {
          a.length > 0 ? setUserAddons(a) : setUserAddons([]);
        });
      })
      .catch((err) => {
        setError(err.message);
      });
  }, [username]);

  const handleUserBlocking = () => {
    setIsBlocked(true);
    setUser((prev) => ({ ...prev, isBlocked: true }));
    return blockUser(user.userName);
  };

  const handleUserUnblocking = () => {
    setIsBlocked(false);
    setUser((prev) => ({ ...prev, isBlocked: false }));
    return unblockUser(user.userName);
  };

  const changeUserRoleToAdmin = () => {
    setUser((prev) => ({ ...prev, role: 2 }));
    return updateUserRole(user.userName, 2);
  };

  const changeUserRoleToDefault = () => {
    setUser((prev) => ({ ...prev, role: 1 }));
    return updateUserRole(user.userName, 1);
  };

  return (
    <>
      {error && (
        <Stack className="error-popup" sx={{ width: '100%' }} spacing={2}>
          <Alert severity="error" variant="filled" onClose={() => setError('')}>
            <AlertTitle>Error</AlertTitle>
            <strong>{error}</strong>
          </Alert>
        </Stack>
      )}
      {!user ? (
        <h1> Loading ....</h1>
      ) : (
        userData && (
          <div>
            <section className="h-100 gradient-custom-2">
              <div className="container py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                  <div
                    className="col col-lg-9 col-xl-7"
                    style={{ width: '950px' }}
                  >
                    <div className="card">
                      <div
                        className="rounded-top text-white d-flex flex-row"
                        style={{ backgroundColor: '#acb4db', height: '200px' }}
                      >
                        <div
                          className="ms-4 mt-5 d-flex flex-column"
                          style={{ width: '150px' }}
                        >
                          <img
                            src={user.userAvatar}
                            alt="generic-placeholder"
                            className="img-fluid img-thumbnail mt-4 mb-2"
                            style={{
                              width: '160px',
                              zIndex: 1,
                              borderRadius: '190px',
                            }}
                          />
                        </div>

                        <div
                          className="ms-3"
                          style={{ marginTop: '130px', color: 'black' }}
                        >
                          <h5>
                            {user.firstName} {user.lastName}
                          </h5>
                          <i>@{user.userName}</i>
                        </div>
                      </div>
                      <div className="event-buttons d-flex">
                        {userData.role === 2 && (
                          <Dropdown className="change-user-role">
                            <Dropdown.Toggle
                              id="dropdown-button-change-role"
                              className="dropdown-btn-change-user-role"
                              variant="secondary"
                            >
                              Change role from:{' '}
                              {user.role === 1 ? 'Basic User' : 'Admin'}
                            </Dropdown.Toggle>
                            <Dropdown.Menu variant="dark">
                              <Dropdown.Item onClick={changeUserRoleToAdmin}>
                                To: Admin
                              </Dropdown.Item>
                              <Dropdown.Divider />
                              <Dropdown.Item onClick={changeUserRoleToDefault}>
                                To: Basic User
                              </Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        )}
                        {!isBlocked &&
                        !user.isBlocked &&
                        userData.role === 2 ? (
                          <Button
                            variant="danger"
                            onClick={handleUserBlocking}
                            className="user-block-btn"
                          >
                            Block
                          </Button>
                        ) : (
                          userData.role === 2 && (
                            <Button
                              variant="success"
                              onClick={handleUserUnblocking}
                              className="user-unblock-btn"
                            >
                              Unblock
                            </Button>
                          )
                        )}
                      </div>
                      <div
                        className="p-4 text-black"
                        style={{ backgroundColor: '#f8f9fa' }}
                      >
                        <div className="d-flex justify-content-end text-center py-1">
                          <div>
                            <p className="mb-1 h5">{userAddons.length}</p>
                            <p className="small text-muted mb-0">
                              Addons Uploaded
                            </p>
                          </div>
                          <div className="px-3">
                            <p className="mb-1 h5">
                              {userAddons.reduce((acc, addon) => {
                                acc += addon.downloads;

                                return acc;
                              }, 0)}
                            </p>
                            <p className="small text-muted mb-0">
                              Total Addon Downloads
                            </p>
                          </div>
                          <div>
                            <p className="mb-1 h5">
                              {userAddons.reduce(
                                (acc, addon, _, userAddons) => {
                                  acc += addon.rating / userAddons.length;

                                  return (
                                    Math.round((acc + Number.EPSILON) * 100) /
                                    100
                                  );
                                },
                                0
                              )}
                            </p>
                            <p className="small text-muted mb-0">
                              Average Addon Rating
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="card-body p-4 text-black">
                        <div className="d-flex justify-content-between align-items-center mb-4">
                          <p
                            className="lead fw-normal mb-0"
                            style={{
                              borderBottom: '2px solid black',
                              width: '100%',
                              textAlign: 'center',
                              fontSize: '35px',
                              fontStyle: 'italic',
                            }}
                          >
                            Uploaded Addons
                          </p>
                        </div>
                        <div className="row g-2">
                          {userAddons.length === 0 ? (
                            <div
                              style={{ width: '100vw', textAlign: 'center' }}
                            >
                              <h5> You have no uploaded addons.</h5>
                            </div>
                          ) : (
                            userAddons.map((addon, key) => (
                              <div
                                className="addons-profile-cards"
                                style={{
                                  marginRight: '50px',
                                  width: '35%',
                                  marginBottom: '15px',
                                  marginLeft: '50px',
                                }}
                                key={key}
                              >
                                <Card
                                  sx={{ maxWidth: 345 }}
                                  style={{
                                    boxShadow: '0 0 2em #ddd',
                                    border: '1px solid lightgray',
                                  }}
                                >
                                  <CardMedia
                                    component="img"
                                    alt="addon-image"
                                    height="200"
                                    src={addon.ImageUrl}
                                    key={key}
                                  />
                                  <CardContent>
                                    <Typography
                                      gutterBottom
                                      variant="h5"
                                      component="div"
                                      style={{ textAlign: 'center' }}
                                    >
                                      {addon.name}
                                    </Typography>
                                    <Accordion>
                                      <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                      >
                                        <Typography>Description</Typography>
                                      </AccordionSummary>
                                      <AccordionDetails>
                                        <Typography>
                                          {addon.description}
                                        </Typography>
                                      </AccordionDetails>
                                    </Accordion>
                                    <Typography
                                      variant="body3"
                                      color="text.secondary"
                                    >
                                      Tags: {addon.tags} <br />
                                    </Typography>
                                    <Typography
                                      variant="body3"
                                      color="text.secondary"
                                    >
                                      IDE: {addon.ide} <br></br>
                                    </Typography>
                                    <Typography
                                      variant="body3"
                                      color="text.secondary"
                                    >
                                      Rating: {addon.rating}
                                      <br></br>
                                    </Typography>
                                    <Typography
                                      variant="body3"
                                      color="text.secondary"
                                    >
                                      Downloads: {addon.downloads}
                                    </Typography>
                                  </CardContent>
                                </Card>
                              </div>
                            ))
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        )
      )}
    </>
  );
}
