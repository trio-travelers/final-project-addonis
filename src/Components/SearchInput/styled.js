import styled from 'styled-components';

export const Input = styled.input`
	width: 300px;
	height: 51px;
	padding: 10px;
	background: #f3f3f3;
	box-shadow: inset 0px 4px 4px rgba(0, 0, 0, 0.1);
	border-radius: 5px;
	border-color: green;
`;

export const Ul = styled.ul`
	display: contents;
`;

export const Li = styled.li`
	width: 300px;
	font-weight: bold;
	height: 51px;
	padding: 10px;
	background: #f5f0f0;
	display: block;
	border-bottom: 1px solid white;
	&:hover {
		cursor: pointer;
		background-color: white;
	}
	z-index: 100;
`;

export const SuggestContainer = styled.div`
	height: 20px;
	width: 300px;
	overflow: scroll;
	&::-webkit-scrollbar {
		display: none;
	}
	-ms-overflow-style: none; /* IE and Edge */
	scrollbar-width: 1px ; /* Firefox */
`;
export const MainWrapper = styled.div`
	display: flex;
	justify-content: center;
	padding: 5px;
`;