import React, { useState } from 'react';
import { Input, Ul, Li, SuggestContainer } from './styled';
import { Dropdown } from 'react-bootstrap';
import './SearchInput.css';

export default function SearchInput({ loading, result, requests, placeholder, searchBy, onClick }) {
  const [inputValue, setInputValue] = useState('');

  const updateValue = (newValue) => {
    setInputValue(newValue);
    requests(newValue);
  };

  return (
    <div>
      <Input value={inputValue} onChange={(input) => updateValue(input.target.value)} placeholder={placeholder} />
      <SuggestContainer style={{ zIndex: 9999, overflow: 'visible' }}>
        <Ul>
          {loading && <Li>Loading...</Li>}
          {result?.length === 0 ? (
            <Li>
              <p> No users found</p>
            </Li>
          ) : (
            !loading &&
            result?.length > 0 &&
            searchBy === 'Email' &&
            result?.map((user, index) => (
              <>
                <Li key={`${user.uid}-${index}`} onClick={onClick}>
                  <img className="dropdown-picture" src={user.userAvatar} alt="dropdown-view" /> {user.email}
                </Li>
                <Dropdown.Divider />
              </>
            ))
          )}
          {!loading &&
            result?.length > 0 &&
            searchBy === 'Username' &&
            result?.map((user, index) => (
              <>
                <Li key={`${user.uid}-${index}`} onClick={onClick}>
                  <img className="dropdown-picture" src={user.userAvatar} alt="dropdown-view" /> {user.userName}
                </Li>
                <Dropdown.Divider />
              </>
            ))}
          {!loading &&
            result?.length > 0 &&
            searchBy === 'Phone Number' &&
            result?.map((user, index) => (
              <>
                <Li key={`${user.uid}-${index}`} onClick={onClick}>
                  <img className="dropdown-picture" src={user.userAvatar} alt="dropdown-view" /> {user.phoneNumber}
                </Li>
                <Dropdown.Divider />
              </>
            ))}
        </Ul>
      </SuggestContainer>
    </div>
  );
}
