import React, { useContext, useEffect, useState } from "react";
import { getAllAddons } from "../../Services/addons.services";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./AddonSlider.css";
import AddonCard from "../AddonCard/AddonCard";

const AddonSlider = () => {
  const [addons, setAddons] = useState();

  useEffect(() => {
    getAllAddons().then(setAddons).catch(console.error);
  }, []);

  const settings = {
    dots: true,
    autoplay: false,
    infinite: false,
    slidesToShow: 6,
    slidesToScroll: 6,
  };

  return (
    <>
      {!addons ? (
        <h1> Loading ....</h1>
      ) : (
        addons && (
          <div className="container-carousel">
            <Slider {...settings}>
            {addons.map((addon, index) => {
          return (
            <AddonCard
              key={index}
              ImageUrl={addon.ImageUrl}
              name={addon.name}
              author={addon.author}
              ide={addon.ide}

            />
          );
        })}
             
            </Slider>
          </div>
        )
      )}
    </>
  );
};
export default AddonSlider;
