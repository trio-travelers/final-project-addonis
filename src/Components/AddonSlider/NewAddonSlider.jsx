import React, { useEffect, useState } from "react";
import { getAllAddons } from "../../Services/addons.services";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import AddonCard from "../AddonCard/AddonCard";

const NewAddonSlider = () => {
  const [addons, setAddons] = useState();

  useEffect(() => {
    getAllAddons().then(setAddons).catch(console.error);
  }, []);

  const settings = {
    dots: true,
    autoplay: false,
    infinite: false,
    slidesToShow: 6,
    slidesToScroll: 6,
  };

  return (
    <>
      {!addons ? (
        <h4> Loading newest addons...</h4>
      ) : (
        addons && (
          <div className="container-carousel">
            <Slider {...settings}>
              {addons
                .filter((e) => e.pending === false)
                .slice(-12)
                .sort((a, b) => b.date - a.date)
                .map((e) => (
                  <AddonCard
                    key={e.name}
                    ImageUrl={e.ImageUrl}
                    name={e.name}
                    author={e.author}
                    ide={e.ide}
                    rating={+e.rating}
                  />
                ))}
            </Slider>
          </div>
        )
      )}
    </>
  );
};

export default NewAddonSlider;
