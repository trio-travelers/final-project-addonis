import React, { useEffect, useState } from 'react';
import { getAllAddons } from '../../Services/addons.services';
import AddonsCards from '../AddonsSampleCards/AddonsCards';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';

export default function AddonsApproval() {
  const [pendingAddons, setPendingAddons] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    getAllAddons()
      .then((addons) => {
        const pendingApprovalAddons = addons.filter((a) => a.pending === true);
        setPendingAddons(pendingApprovalAddons);
      })
      .catch((err) => setError(err.message));
  }, []);

  const approve = (a) =>
    setPendingAddons((prev) => prev.filter((addon) => addon.id !== a.id));

  const decline = (a) =>
    setPendingAddons((prev) => prev.filter((addon) => addon.id !== a.id));

  return (
    <>
      {error && (
        <Stack sx={{ width: '100%' }} spacing={2}>
          <Alert severity="error" variant="outlined" onClose={() => {}}>
            <AlertTitle>Error</AlertTitle>
            <strong>{error}</strong>
          </Alert>
        </Stack>
      )}
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'space-between',
        }}
      >
        {pendingAddons.length > 0 ? (
          pendingAddons.map((a) => {
            return (
              <AddonsCards
                key={a.id}
                id={a.id}
                handleApprove={() => approve(a)}
                handleDecline={() => decline(a)}
                file={a.fileName}
                addonImage={a.imageName}
                image={a.ImageUrl}
                name={a.name}
                description={a.description}
                tags={a.tags}
                ide={a.ide}
              />
            );
          })
        ) : (
          <h1
            style={{
              marginTop: '20px',
              marginBottom: '400px',
              marginLeft: '560px',
            }}
          >
            {' '}
            No addons are pending approval
          </h1>
        )}
      </div>
    </>
  );
}
