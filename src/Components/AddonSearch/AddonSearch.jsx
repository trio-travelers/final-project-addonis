import React, {useState, useEffect} from 'react';
import './AddonSearch.css';

function Search() {
    const [value, setValue] = useState('');
    const [result, setResult] = useState([]);

    useEffect(() => {
        if (value.length > 0) {
            fetch(
                'https://final-project-adonis-default-rtdb.europe-west1.firebasedatabase.app/addons.json',
            )
                .then((response) => response.json())
                .then((responseData) => {
                    setResult([]);
                    const searchQuery = value.toLowerCase();
                    for (const key in responseData) {
                        const word = responseData[key].name.toLowerCase();
                        const text = responseData[key].tags.toLowerCase();
                        if (
                            word.includes(searchQuery) ||
                            text.includes(searchQuery)
                        ) {
                            responseData[key].id = key;
                            setResult((prevResult) => {
                                return [...prevResult, responseData[key]];
                            });
                        }
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        } else {
            setResult([]);
        }
    }, [value]);


    return (
        <div>
            <input
                type="text"
                className="searchBar"
                placeholder=" Search in all addons"
                onChange={(event) => setValue(event.target.value)}
                value={value}
            />
            <div className="searchBack">
                {result.map((result, index) => (
                    <a href={'/addons/' + result.id} key={index}>
                        <div className="searchEntry">{result.name}</div>
                    </a>
                ))}
            </div>
        </div>
    );
}

export default Search;