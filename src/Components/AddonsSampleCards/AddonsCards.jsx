import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { approveAddon, deleteAddon } from '../../Services/addons.services';

export default function AddonsCards({
  image,
  name,
  description,
  tags,
  ide,
  id,
  file,
  addonImage,
  handleApprove,
  handleDecline,
}) {
  const handleAddonApproval = () => {
    approveAddon(id);
    handleApprove();
  };

  const handleAddonDecline = () => {
    deleteAddon(id, file, addonImage);
    handleDecline();
  };

  return (
    <div
      className="addons-cards"
      style={{
        marginLeft: '60px',
        marginBottom: '40px',
        marginTop: '50px',
        marginRight: '40px',
        width: '20%',
      }}
    >
      <Card
        sx={{ maxWidth: 345 }}
        style={{ boxShadow: '0 0 2em #ddd', border: '1px solid lightgray' }}
      >
        <CardMedia component="img" alt="addon-image" height="200" src={image} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {name}
          </Typography>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>Description</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>{description}</Typography>
            </AccordionDetails>
          </Accordion>
          <Typography variant="body3" color="text.secondary">
            Tags: {tags} <br />
          </Typography>
          <Typography variant="body3" color="text.secondary">
            IDE: {ide}
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            className="approve-btn"
            size="small"
            onClick={handleAddonApproval}
          >
            Approve
          </Button>
          <Button
            className="decline-btn"
            size="small"
            style={{ marginLeft: '175px' }}
            onClick={handleAddonDecline}
          >
            Decline
          </Button>
        </CardActions>
      </Card>
    </div>
  );
}
