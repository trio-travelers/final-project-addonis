import React from 'react';
import './AboutUs.css';
import Mario from '../../images/Mario.png';
import Asya from '../../images/Asya_.jpg';
import Vesko from '../../images/Vesko.jpg';
import GitLabLogo from '../../images/gitlab.png';
import CVDocument from '../../images/CV.png';
export default function AboutUs() {
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-12 col-sm-8 col-lg-6">
          <div
            className="section_heading text-center wow fadeInUp"
            data-wow-delay="0.2s"
            style={{
              visibility: 'visible',
              animationDelay: '0.2s',
              animationName: 'fadeInUp',
            }}
          >
            <div className="line"></div>
          </div>
        </div>
      </div>
      <div className="row">
        <div
          className="col-12 col-sm-6 col-lg-3"
          style={{ marginLeft: '-150px' }}
        >
          <div
            className="single_advisor_profile wow fadeInUp"
            data-wow-delay="0.2s"
            style={{
              visibility: 'visible',
              animationDelay: '0.2s',
              animationName: 'fadeInUp',
            }}
          >
            <div className="advisor_thumb">
              <img src={Mario} alt="" />

              <div className="social-info">
                <a
                  href="https://gitlab.com/Swayy9"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    src={GitLabLogo}
                    alt="gitlab-logo"
                    style={{ width: '25px', height: '25px' }}
                  />
                </a>
                <a
                  href="https://app.enhancv.com/share/9a7ede07/?utm_medium=growth&utm_campaign=share-resume&utm_source=dynamic"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    src={CVDocument}
                    alt="cv-document"
                    style={{ width: '25px', height: '25px' }}
                  />
                </a>
              </div>
            </div>

            <div className="single_advisor_details_info">
              <h6>Mario Vangelov</h6>
              <p className="designation">Front-End Developer</p>
            </div>
          </div>
        </div>

        <div
          className="col-12 col-sm-6 col-lg-3"
          style={{ marginLeft: '200px' }}
        >
          <div
            className="single_advisor_profile wow fadeInUp"
            data-wow-delay="0.3s"
            style={{
              visibility: 'visible',
              animationDelay: '0.2s',
              animationName: 'fadeInUp',
            }}
          >
            <div className="advisor_thumb">
              <img
                src={Asya}
                style={{ width: '290px', height: '435px' }}
                alt=""
              />

              <div className="social-info">
                <a
                  href="https://gitlab.com/igova"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    src={GitLabLogo}
                    alt="gitlab-logo"
                    style={{ width: '25px', height: '25px' }}
                  />
                </a>
                <a
                  href="https://app.enhancv.com/share/c4eec6be/?utm_medium=growth&utm_campaign=share-resume&utm_source=dynamic"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    src={CVDocument}
                    alt="cv-document"
                    style={{ width: '25px', height: '25px' }}
                  />
                </a>
              </div>
            </div>

            <div className="single_advisor_details_info">
              <h6>Asya Igova</h6>
              <p className="designation">Front-End Developer</p>
            </div>
          </div>
        </div>

        <div
          className="col-12 col-sm-6 col-lg-3"
          style={{ marginLeft: '270px' }}
        >
          <div
            className="single_advisor_profile wow fadeInUp"
            data-wow-delay="0.5s"
            style={{
              visibility: 'visible',
              animationDelay: '0.2s',
              animationName: 'fadeInUp',
            }}
          >
            <div className="advisor_thumb">
              <img
                src={Vesko}
                alt=""
                style={{ width: '300px', height: '435px' }}
              />

              <div className="social-info">
                <a
                  href="https://gitlab.com/VeselinShterev"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    src={GitLabLogo}
                    alt="gitlab-logo"
                    style={{ width: '25px', height: '25px' }}
                  />
                </a>
                <a
                  href="https://app.enhancv.com/share/600bdedc/?utm_medium=growth&utm_campaign=share-resume&utm_source=dynamic"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    src={CVDocument}
                    alt="cv-document"
                    style={{ width: '25px', height: '25px' }}
                  />
                </a>
              </div>
            </div>

            <div className="single_advisor_details_info">
              <h6>Veselin Shterev</h6>
              <p className="designation">Front-End Developer</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
