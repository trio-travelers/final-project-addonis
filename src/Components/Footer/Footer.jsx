import React from 'react';
import './Footer.css';
import TeamLogo from '../../images/teamLogo.png';

export default function Footer() {
  return (
    <div className="container-fluid mt-5" style={{ width: '100%' }}>
      <div className="card " style={{ backgroundColor: '#acb4db' }}>
        <div className="row mb-4">
          <div className="col-md-4 col-sm-4 col-xs-4">
            <div className="footer-text pull-left">
              <img
                src={TeamLogo}
                alt="team-logo"
                style={{ width: '240px', height: '200px' }}
              />
            </div>
          </div>

          <div className="col-md-2 col-sm-2 col-xs-2">
            <h5 className="heading">Information</h5>
            <ul>
              <a href="/FAQ">FAQ</a> <br></br>
              <a
                href="https://gitlab.com/trio-travelers/final-project-addonis"
                target="_blank"
                rel="noreferrer"
              >
                GitLab Repository
              </a>{' '}
              <br></br>
              <a href="/terms-and-conditions">Terms of use</a>
            </ul>
          </div>
          <div className="col-md-2 col-sm-2 col-xs-2">
            <h5 className="heading">Technologies</h5>
            <ul className="card-text">
              <a href="https://reactjs.org/" target="_blank" rel="noreferrer">
                React
              </a>{' '}
              <br></br>
              <a
                href="https://getbootstrap.com/"
                target="_blank"
                rel="noreferrer"
              >
                Bootstrap
              </a>{' '}
              <br></br>
              <a href="https://mui.com/" target="_blank" rel="noreferrer">
                Material UI
              </a>
            </ul>
          </div>
          <div className="col-md-2 col-sm-2 col-xs-2">
            <h5 className="heading">Team</h5>
            <ul className="card-text">
              <a href="/about-us">About Us</a> <br></br>
            </ul>
          </div>
        </div>
        <div className="divider mb-3"> </div>
        <div className="row" style={{ fontSize: '10px' }}>
          <div className="col-md-2 col-sm-2 col-xs-2">
            <div className="pull-left">
              <p>
                <i className="fa fa-copyright"></i> 2022@ The Addon Buddies
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
