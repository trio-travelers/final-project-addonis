import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  getAddonByName,
  updateAddonDescription,
  updateAddonIde,
  updateAddonTags,
  updateAddonTitle,
} from "../../Services/addons.services";
import { useQuery } from "react-query";
import {
  Box,
  Button,
  Card,
  Grid,
  Paper,
  TextareaAutosize,
  Typography,
} from "@mui/material";

export const UpdateAddon = () => {
  const params = useParams();
  const [addons, setAddons] = useState();
  const [description, setDescription] = useState("");
  const [title, setTitle] = useState("");
  const [tags, setTags] = useState("");
  const [ide, setIde] = useState("");

  const navigate = useNavigate();

  const { data: addon } = useQuery(["addonData", params], () =>
    getAddonByName(params.name).then(setAddons)
  );

  return (
    addons && (
      <div>
        <Paper
          sx={{
            p: 2,
            margin: "auto",
            maxWidth: 1030,
            flexGrow: 1,
            alignContent: "center",
            backgroundColor: "#acb4db",
          }}
        >
          <Card sx={{ minWidth: 275, backgroundColor: "#7a9ecb" }}>
            <Grid container spacing={2}>
              <Grid item xs={4}>
                <Box sx={{ mx: "auto", width: 200, textAlign: "center" }}>
                  {!addons.ImageUrl ? (
                    <img
                      className="imageSlider"
                      src={
                        "https://findicons.com/files/icons/653/the_spherical/128/file_download.png"
                      }
                      alt="basic"
                    />
                  ) : (
                    <img
                      className="imageSlider"
                      src={addons.ImageUrl}
                      alt={addons.description}
                    />
                  )}
                </Box>
              </Grid>
              <Grid
                item
                xs={4}
                sx={{ mt: 2, textAlign: "center", color: "#1f3656" }}
              >
                <Typography component="div" fontFamily="Kanit" variant="h6">
                  Edit details for
                </Typography>
                <Typography
                  component="div"
                  fontFamily="Kanit"
                  variant="h4"
                  sx={{ color: "#1f3656" }}
                >
                  {addons.name}
                </Typography>
              </Grid>
              <Grid item xs={4} sx={{ mt: 2, textAlign: "center" }}></Grid>
            </Grid>
          </Card>
          <br></br>
          <TextareaAutosize
            defaultValue={addons.name}
            aria-label="minimum height"
            minRows={1}
            onChange={(event) => setTitle(event.target.value)}
            style={{ width: 1000, backgroundColor: "#bbcee4" }}
          />
          <Button
            variant="contained"
            size="small"
            sx={{
              backgroundColor: "#5a9fe0",
            }}
            onClick={() =>
              updateAddonTitle(addons.id, title).then(
                navigate(`/addon/${title}`)
              )
            }
          >
            Edit Name
          </Button>
          <br></br>
          <br></br>
          <TextareaAutosize
            defaultValue={addons.tags}
            aria-label="minimum height"
            minRows={1}
            onChange={(event) => setTags(event.target.value)}
            style={{ width: 1000, backgroundColor: "#bbcee4" }}
          />
          <Button
            variant="contained"
            size="small"
            sx={{
              backgroundColor: "#5a9fe0",
            }}
            onClick={() =>
              updateAddonTags(addons.id, tags).then(navigate(navigate(-1)))
            }
          >
            Edit Tags
          </Button>
          <br></br>
          <br></br>
          <TextareaAutosize
            defaultValue={addons.ide}
            aria-label="minimum height"
            minRows={1}
            onChange={(event) => setIde(event.target.value)}
            style={{ width: 1000, backgroundColor: "#bbcee4" }}
          />
          <Button
            variant="contained"
            size="small"
            sx={{
              backgroundColor: "#5a9fe0",
            }}
            onClick={() =>
              updateAddonIde(addons.id, ide).then(navigate(navigate(-1)))
            }
          >
            Edit IDE
          </Button>
          <br></br>
          <br></br>
          <TextareaAutosize
            defaultValue={addons.description}
            aria-label="minimum height"
            minRows={3}
            onChange={(event) => setDescription(event.target.value)}
            style={{ width: 1000, backgroundColor: "#bbcee4" }}
          />
          <Button
            variant="contained"
            size="small"
            sx={{
              backgroundColor: "#5a9fe0",
            }}
            onClick={() =>
              updateAddonDescription(addons.id, description).then(navigate(-1))
            }
          >
            Edit Description
          </Button>
        </Paper>
      </div>
    )
  );
};
