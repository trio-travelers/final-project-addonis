import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  getAddonByName,
  updateDownloads,
  deleteAddon,
} from "../../Services/addons.services";
import {
  getRepoInfo,
  getCommitInfo,
  pullsCount,
} from "../../Services/github.services";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import { useQuery } from "react-query";
import { Link } from "@mui/material";
import GitHubIcon from "@mui/icons-material/GitHub";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import AccountTreeIcon from "@mui/icons-material/AccountTree";
import WarningIcon from "@mui/icons-material/Warning";
import AutoStoriesIcon from "@mui/icons-material/AutoStories";
import AppContext from "../../Providers/App.Context";
import { RatingAddon } from "../RatingAddon/RatingAddons";
import "./AddonItem.css"

const AddonItem = () => {
  const params = useParams();
  const { userData } = useContext(AppContext);
  const [addons, setAddons] = useState();
  let [downloads, setDownloads] = useState();

  const navigate = useNavigate();

  const { data: addon } = useQuery(["addonData", params], () =>
    getAddonByName(params.name).then(setAddons)
  );

  useEffect(() => {
    if (addons) {
      setDownloads(addons.downloads);
    }
  }, [addons]);

  const [info, setInfo] = useState();
  useEffect(() => {
    if (addons) {
      getRepoInfo(`${addons.repoOwner}/${addons.repoName}`)
        .then(setInfo)
        .catch(console.error);
    }
  }, [addons]);

  const [pulls, setPulls] = useState();
  useEffect(() => {
    if (!addons) return;
    pullsCount(`${addons.repoOwner}/${addons.repoName}`)
      .then(setPulls)
      .catch(console.error);
  }, [addons]);

  const [commits, setCommits] = useState();
  useEffect(() => {
    if (!addons) return;
    getCommitInfo(`${addons.repoOwner}/${addons.repoName}`)
      .then(setCommits)
      .catch(console.error);
  }, [addons]);

  const handleDownloads = () => {
    const value = addons.downloads + 1;
    updateDownloads(addons.id, value);
  };

  const handleDelete = async () => {
    await deleteAddon(addons.id, addons.fileName, addons.imageName);
    navigate("/home");
  };

  const isUserAuthor = () => addons.author === userData.userName;

  return (
    addons &&
    info &&
    commits && (
      <div className="container" key={addons.description}>
        <br></br>
        <br></br>
        <Card
          sx={{
            minWidth: 275,
            backgroundColor: "#acb4db",
          }}
        >
          <Grid container spacing={2}>
            <Grid item xs={4}>
              <Box sx={{ mx: "auto", width: 210, textAlign: "center" }}>
                {!addons.ImageUrl ? (
                  <img
                    className="imageSlider"
                    src={
                      "https://findicons.com/files/icons/653/the_spherical/128/file_download.png"
                    }
                    alt="basic"
                  />
                ) : (
                  <img
                    className="imageSlider"
                    src={addons.ImageUrl}
                    alt={addons.description}
                  />
                )}
              </Box>
            </Grid>
            <Grid item xs={4} sx={{ mt: 2, textAlign: "center" }}>
              <Typography component="div" fontFamily="Kanit" variant="h4">
                {addons.name}
              </Typography>
              <Typography
                component="div"
                variant="h6"
                fontFamily="Kanit"
                sx={{ color: "#22528d", ":hover": {
                  cursor: "pointer"
                }, marginBottom: "5px"}}
                onClick={() => navigate(`/users/${addons.author}`)}
              >
                {addons.author}
              </Typography>
              <Button
                onClick={() => handleDownloads()}
                href={`${addons.downloadUrl}`}
                variant="contained"
                size="small"
                sx={{
                  backgroundColor: "#5a9fe0", marginBottom: "10px"
                }}
              >
                Download
              </Button>
            </Grid>
            <Grid item xs={4} sx={{ mt: 2, textAlign: "center" }}>
              <Box sx={{ mx: "auto", width: 200, textAlign: "center" }}>
                <RatingAddon addon={addons} />
              </Box>
            </Grid>
          </Grid>
        </Card>
        <Grid container spacing={2} sx={{ px: 10, py: 5 }}>
          <Grid item xs={12} sx={{ pt: 10 }}>
            <Typography
              component="div"
              variant="h6"
              fontFamily="Kanit"
              mb={"20px"}
              sx={{
                color: "#22528d",
                fontStyle: "italic",
                textAlign: "center",
              }}
            >
              Short description of the {addons.name} addon:
            </Typography>
            <Typography
              component="div"
              variant="body1"
              fontFamily="Kanit"
              sx={{ textAlign: "center" }}
            >
              {addons.description}
            </Typography>
            <br></br>
            <Grid
              item
              fontFamily="Kanit"
              xs={12}
              sx={{ color: "#22528d", textAlign: "center" }}
            >
              <Link
                href={`https://github.com/${addons.repoOwner}/${addons.repoName}/blob/master/README.md`}
                target="_blank"
              >
                <AutoStoriesIcon />
                &nbsp;Check GitHub ReadMe file for more details
              </Link>
            </Grid>
          </Grid>
          <Grid item xs={5}>
            <Typography
              component="div"
              variant="body2"
              fontFamily="Kanit"
              mt={"40px"}
              sx={{
                color: "#22528d",
                fontStyle: "italic",
                textAlign: "right",
              }}
            >
              Tags:
            </Typography>
          </Grid>
          <Grid item xs={7} mt={"40px"}>
            {addons.tags.split(",").map((el, index) => (
              <Button
                href={`/addons?tag=${el}`}
                variant="outlined"
                size="small"
                sx={{ ml: 2 }}
                key={index}
              >
                {el}
              </Button>
            ))}
          </Grid>
          <Grid item xs={5}>
            <Typography
              component="div"
              variant="body2"
              fontFamily="Kanit"
              mt={"10px"}
              sx={{
                color: "#22528d",
                fontStyle: "italic",
                textAlign: "right",
              }}
            >
              IDE:
            </Typography>
          </Grid>
          <Grid item xs={7} mt={"10px"}>
            <Button
              href={`/addons?ide=${addons.ide}`}
              variant="outlined"
              size="small"
              sx={{ ml: 2 }}
              key={addons.ide}
            >
              {addons.ide}
            </Button>
          </Grid>
          <Grid item xs={5}>
            <Typography
              component="div"
              variant="body2"
              fontFamily="Kanit"
              sx={{
                color: "#22528d",
                fontStyle: "italic",
                textAlign: "right",
              }}
            >
              Downloads:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <Typography fontFamily="Kanit" sx={{ ml: 2, color: "#22528d" }}>
              {addons.downloads}
            </Typography>
          </Grid>
          <Grid item xs={5}>
            <Typography
              component="div"
              variant="body2"
              fontFamily="Kanit"
              sx={{
                color: "#22528d",
                fontStyle: "italic",
                textAlign: "right",
              }}
            >
              Works with:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <Typography fontFamily="Kanit" sx={{ ml: 2, color: "#22528d" }}>
              {addons.ide}
            </Typography>
          </Grid>
          <Grid item xs={5}>
            <Typography
              component="div"
              variant="body2"
              fontFamily="Kanit"
              sx={{
                color: "#22528d",
                fontStyle: "italic",
                textAlign: "right",
              }}
            >
              Project description:
            </Typography>
          </Grid>
          <Grid item fontFamily="Kanit" xs={7} sx={{ color: "#22528d" }}>
            <Link href={info.html_url} target="_blank">
              <GitHubIcon />
              &nbsp;GitHub repo
            </Link>
          </Grid>
          <Grid item xs={5}></Grid>
          <Grid item fontFamily="Kanit" xs={7} sx={{ color: "#22528d" }}>
            <Link
              href={`https://github.com/${addons.repoOwner}/${addons.repoName}/commits`}
              target="_blank"
            >
              <AccessTimeIcon />
              &nbsp;Date of the last commit: {commits.split("T", 1)}
            </Link>
          </Grid>
          <Grid item xs={5}></Grid>
          <Grid item fontFamily="Kanit" xs={7} sx={{ color: "#22528d" }}>
            <Link
              href={`https://github.com/${addons.repoOwner}/${addons.repoName}/pulls`}
              target="_blank"
            >
              <AccountTreeIcon />
              &nbsp;{pulls} pull requests
            </Link>
          </Grid>
          <Grid item xs={5}></Grid>
          <Grid item fontFamily="Kanit" xs={7} sx={{ color: "#22528d" }}>
            <Link
              href={`https://github.com/${addons.repoOwner}/${addons.repoName}/issues`}
              target="_blank"
            >
              <WarningIcon />
              &nbsp;{info.open_issues} open issues
            </Link>
          </Grid>
          <Grid item xs={12} />
          {(userData.role === 2 || (userData && isUserAuthor())) && (
            <Grid item xs={12} sx={{ textAlign: "center" }}>
              <Button
                href={`/addons/${addons.author}/${addons.name}`}
                variant="contained"
                size="small"
                sx={{
                  backgroundColor: "#5a9fe0",
                  mr: "30px",
                }}
              >
                Edit Addon
              </Button>
              <Button
                onClick={() => handleDelete()}
                variant="contained"
                size="small"
                sx={{
                  backgroundColor: "#5a9fe0",
                  mr: "30px",
                }}
              >
                Delete Addon
              </Button>
            </Grid>
          )}
        </Grid>
      </div>
    )
  );
};

export default AddonItem;
