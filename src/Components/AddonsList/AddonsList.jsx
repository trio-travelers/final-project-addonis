import React, { useEffect } from 'react';
import { useState } from 'react';
import { getAllAddons } from '../../Services/addons.services';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import './AddonsList.css';
import { useNavigate } from 'react-router-dom';

export default function AddonsList() {
  const [addons, setAddons] = useState([]);
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const columns = [
    {
      field: 'ImageUrl',
      headerName: 'Addon Avatar',
      width: 100,
      flex: 1,
      renderCell: (params) => {
        return (
          <div>
            <img src={params.row.ImageUrl} className="addonAvatar" alt="" />
          </div>
        );
      },
    },
    { field: 'name', headerName: 'Addon Name', width: 130, flex: 1 },
    { field: 'author', headerName: 'Author', width: 100, flex: 1 },
    { field: 'downloads', headerName: 'Downloads', width: 90, flex: 1 },
    { field: 'ide', headerName: 'IDE', width: 100, flex: 1 },
    { field: 'tags', headerName: 'Tags', width: 120, flex: 1 },
    {
      field: 'pending',
      headerName: 'Pending Approval',
      width: 90,
      flex: 1,
      renderCell: (params) => {
        return (
          <div>
            {params.row.pending === true ? (
              <DoneIcon color="primary" />
            ) : (
              <ClearIcon color="primary" />
            )}
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    getAllAddons()
      .then((a) => {
        setAddons(a);
      })
      .catch((err) => setError(err.message));
  }, []);

  const navigateToAddonView = (addonName) => {
    return navigate(`/addon/${addonName}`);
  };

  return (
    <>
      {error && (
        <Stack className="error-popup" sx={{ width: '100%' }} spacing={2}>
          <Alert severity="error" variant="filled" onClose={() => setError('')}>
            <AlertTitle>Error</AlertTitle>
            <strong>{error}</strong>
          </Alert>
        </Stack>
      )}
      <div
        style={{
          height: '450px',
          maxWidth: '75%',
          margin: '2em auto',
          backgroundColor: '#acb4db',
        }}
      >
        <DataGrid
          pageSize={5}
          rowsPerPageOptions={[5]}
          onCellClick={(params) => navigateToAddonView(params.row.name)}
          rows={addons}
          columns={columns}
          getRowId={(a) => a.id}
          components={{ Toolbar: GridToolbar }}
        />
      </div>
    </>
  );
}
