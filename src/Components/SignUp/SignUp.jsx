import React from "react";
import { useState } from "react";
import { registerUser } from "../../Services/auth.services";
import {
  additionalUserInfo,
  getUserByUsername,
} from "../../Services/user.services";
import { Card, Form, Button, Container } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import PhoneInputWithCountrySelect, {
  isValidPhoneNumber,
} from "react-phone-number-input";
import "react-phone-number-input/style.css";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import Stack from "@mui/material/Stack";

export default function SignUp() {
  const [userPhoneNumber, setUserPhoneNumber] = useState("");

  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    userName: "",
  });

  const [message, setMessage] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setUser({
      ...user,
      [prop]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (user.userName.length < 2 || user.userName.length > 20) {
      return setError("User name must be between 2 and 20 symbols");
    }

    if (userPhoneNumber.length > 14 || userPhoneNumber.length < 10) {
      return setError(
        "Your phone number must be between 10 and 14 digits long"
      );
    }

    if (user.password !== user.confirmPassword) {
      return setError("Password does not match");
    }

    getUserByUsername(user.userName).then((snapshot) => {
      if (snapshot.exists()) {
        return setError(`${user.userName} is already taken`);
      }
      return registerUser(user.email, user.password)
        .then((u) => {
          setMessage("Successfully registered, navigating you to home page");
          setError("");
          setTimeout(() => {
            navigate("/home");
          }, 3000);
          return additionalUserInfo(
            user.userName,
            u.user.uid,
            u.user.email,
            userPhoneNumber,
            user.firstName,
            user.lastName
          );
        })
        .catch((err) => setError(`${err.message}`));
    });
  };
  return (
    <>
      <div>
        <Container
          className="d-flex align-items-center justify-content-center"
          style={{ minHeight: "100vh" }}
        >
          <div className="w-100" style={{ maxWidth: "400px" }}>
            <Card style={{ backgroundColor: "rgba(97,121,157,.9)" }}>
              <Card.Body>
                <h2 className="text-center mb-4" style={{ color: "#1f3656" }}>Sign up</h2>
                {error && (
                  <Stack
                    className="error-popup"
                    sx={{ width: "100%" }}
                    spacing={2}
                  >
                    <Alert
                      severity="error"
                      variant="filled"
                      onClose={() => setError("")}
                    >
                      <AlertTitle>Error</AlertTitle>
                      <strong>{error}</strong>
                    </Alert>
                  </Stack>
                )}
                {message && (
                  <Stack
                    className="message-popup"
                    sx={{ width: "100%" }}
                    spacing={2}
                  >
                    <Alert
                      severity="success"
                      variant="filled"
                      onClose={() => setMessage("")}
                    >
                      <AlertTitle>Success</AlertTitle>
                      <strong>{message}</strong>
                    </Alert>
                  </Stack>
                )}
                <Form onSubmit={handleSubmit}>
                  <Form.Group id="firstName">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>First Name</Form.Label>
                    <Form.Control
                      type="text"
                      style={{ opacity: "70%" }}
                      value={user.firstName}
                      onChange={updateForm("firstName")}
                      required
                    />
                  </Form.Group>
                  <Form.Group id="lastName">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>Last Name</Form.Label>
                    <Form.Control
                      type="text"
                      style={{ opacity: "70%" }}
                      value={user.lastName}
                      onChange={updateForm("lastName")}
                      required
                    />
                  </Form.Group>
                  <Form.Group id="username">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>Username</Form.Label>
                    <Form.Control
                      type="text"
                      style={{ opacity: "70%" }}
                      value={user.userName}
                      onChange={updateForm("userName")}
                      required
                    />
                  </Form.Group>
                  <Form.Group id="email">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>Email</Form.Label>
                    <Form.Control
                      type="email"
                      style={{ opacity: "70%" }}
                      value={user.email}
                      onChange={updateForm("email")}
                      required
                    />
                  </Form.Group>
                  <Form.Group id="phonenumber">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>Phone Number</Form.Label>
                    <PhoneInputWithCountrySelect
                      international={true}
                      defaultCountry="BG"
                      placeholder="Enter phone number"
                      value={userPhoneNumber}
                      onChange={setUserPhoneNumber}
                      rules={{ required: true, validate: isValidPhoneNumber }}
                      error={
                        userPhoneNumber
                          ? isValidPhoneNumber(userPhoneNumber)
                            ? undefined
                            : "Invalid phone number"
                          : "Phone number required"
                      }
                    />
                  </Form.Group>
                  <Form.Group id="password">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>Password</Form.Label>
                    <Form.Control
                      type="password"
                      style={{ opacity: "70%" }}
                      value={user.password}
                      onChange={updateForm("password")}
                      required
                    />
                  </Form.Group>
                  <Form.Group id="confirm-password">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>
                      Confirm Password
                    </Form.Label>
                    <Form.Control
                      type="password"
                      style={{ opacity: "70%" }}
                      value={user.confirmPassword}
                      onChange={updateForm("confirmPassword")}
                      required
                    />
                  </Form.Group>
                  <Button
                    className="w-100 mt-4"
                    style={{
                      backgroundColor: "#6399c6",
                      mr: "-200px",
                      borderColor: "#1f3656",
                    }}
                    type="submit"
                  >
                    Sign Up
                  </Button>
                </Form>
                <div className="w-100 text-center mt-2 h6" style={{ color: "#1f3656" }}>
                  Already have an account ?{" "}
                  <Link to="/login" style={{ color: "#96a8d4" }}>
                    Log in
                  </Link>
                </div>
              </Card.Body>
            </Card>
          </div>
        </Container>
      </div>
    </>
  );
}
