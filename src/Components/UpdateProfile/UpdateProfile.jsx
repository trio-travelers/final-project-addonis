import React, { useContext, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import AppContext from '../../Providers/App.Context';
import { storage } from '../../Config/Firebase-config';
import { updatePassword } from 'firebase/auth';
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from 'firebase/storage';
import {
  updateUserFirstName,
  updateUserLastName,
  updateUserProfilePicture,
} from '../../Services/user.services';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import './UpdateProfile.css';

export default function UpdateProfile() {
  const [error, setError] = useState('');
  const { userData, setContext, user } = useContext(AppContext);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');

  const [updateUser, setUpdateUser] = useState({
    password: '',
    firstName: '',
    lastName: '',
  });

  const updateForm = (prop) => (e) => {
    setUpdateUser({
      ...updateUser,
      [prop]: e.target.value,
    });
  };
  function handleSubmitUpdate(e) {
    e.preventDefault();

    setLoading(true);
    setError('');

    if (updateUser.firstName !== userData.firstName) {
      updateUserFirstName(userData.userName, updateUser.firstName);
    }

    if (updateUser.lastName !== userData.lastName) {
      updateUserLastName(userData.userName, updateUser.lastName);
    }

    if (updateUser.password) {
      updatePassword(user, updateUser.password);
    }
  }

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];

    if (!file) return setError(`Please select a file!`);

    const picture = storageRef(storage, `images/${userData.userName}/avatar`);

    uploadBytes(picture, file)
      .then(async (snapshot) => {
        const url = await getDownloadURL(snapshot.ref);
        await updateUserProfilePicture(userData.userName, url);
        setContext({
          user,
          userData: {
            ...userData,
            userAvatar: url,
          },
        });
      })
      .catch((error) => setError(`${error.message}`));
  };

  if (!userData) return null;

  return (
    <div className="row">
      <div className="col-xl-4">
        <div className="card mb-4 mb-xl-0">
          <div className="card-header">Profile Picture</div>
          <div className="card-body text-center">
            <img
              className="img-account-profile"
              src={userData.userAvatar}
              alt="profile-img-update"
            />
            <Form onSubmit={uploadPicture}>
              <input
                type="file"
                name="file"
                id="file"
                className="choose-file"
              ></input>
              <Button type="submit">Submit picture</Button>
            </Form>
          </div>
        </div>
      </div>

      <div className="col-xl-8">
        <div className="card mb-4">
          <div className="card-header">Change Account Details</div>
          <div className="card-body">
            <Form onSubmit={handleSubmitUpdate}>
              <Form.Group id="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  type="text"
                  value={updateUser?.firstName}
                  onChange={updateForm('firstName')}
                  placeholder={userData?.firstName}
                />
              </Form.Group>
              <Form.Group id="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                  type="text"
                  value={updateUser?.lastName}
                  onChange={updateForm('lastName')}
                  placeholder={userData?.lastName}
                />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  value={updateUser?.password}
                  onChange={updateForm('password')}
                  required
                />
              </Form.Group>
              <Button disabled={loading} className="w-20 mt-4" type="submit">
                Update Profile
              </Button>
            </Form>
            {error && (
              <Stack className="error-popup" sx={{ width: '100%' }} spacing={2}>
                <Alert
                  severity="error"
                  variant="filled"
                  onClose={() => setError('')}
                >
                  <AlertTitle>Error</AlertTitle>
                  <strong>{error}</strong>
                </Alert>
              </Stack>
            )}
            {message && (
              <Stack
                className="message-popup"
                sx={{ width: '100%', marginTop: '20px' }}
                spacing={2}
              >
                <Alert
                  severity="success"
                  variant="filled"
                  onClose={() => setMessage('')}
                >
                  <AlertTitle>Success</AlertTitle>
                  <strong>{message}</strong>
                </Alert>
              </Stack>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
