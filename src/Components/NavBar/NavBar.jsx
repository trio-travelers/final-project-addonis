import React, { useContext } from 'react';
import { Navbar, Container, Dropdown } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import AppContext from '../../Providers/App.Context';
import { logoutUser } from '../../Services/auth.services';
import { BoxArrowRight } from 'react-bootstrap-icons';
import './NavBar.css';
import { Button, IconButton, TextField } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { useState } from 'react';

export default function NavBar() {
  const navigate = useNavigate();

  const [searchQuery, setSearchQuery] = useState();

  const { userData, setContext } = useContext(AppContext);

  async function userLogOut() {
    await logoutUser()
      .then(() => {
        setContext({
          user: null,
          userData: null,
        });
      })
      .catch(console.log(Error));

    navigate('/home');
  }

  return (
    <Navbar expand="lg" style={{ zIndex: 9999 }} id="navbar">
      <Container fluid>
        <div className="d-flex">
          <Button
            variant="contained"
            onClick={() => navigate('/home')}
            fontFamily="Kanit"
            className="nav-navbar-btn"
            style={{ marginLeft: '10px', marginRight: '20px' }}
          >
            Home
          </Button>
          {userData ? (
            <Button
              variant="contained"
              onClick={() => navigate('/upload')}
              fontFamily="Kanit"
              className="nav-navbar-btn"
              style={{ marginRight: '20px' }}
            >
              Upload Addon
            </Button>
          ) : null}

          <Button
            variant="contained"
            fontFamily="Kanit"
            className="nav-navbar-btn"
            onClick={() => navigate('/addons-list')}
          >
            Addons
          </Button>
        </div>

        <form>
          <TextField
            id="search-bar"
            className="text"
            onInput={(e) => {
              setSearchQuery(e.target.value);
            }}
            placeholder="Find addon by name, tag or IDE"
            size="small"
            style={{
              width: '400px',
              backgroundColor: '#5a9fe0',
              borderRadius: '4px',
            }}
            onKeyPress={() => navigate(`/addons?search=${searchQuery}`)}
          />
          <IconButton
            href={`/addons?search=${searchQuery}`}
            aria-label="search"
          >
            <SearchIcon style={{ fill: 'white' }} />
          </IconButton>
        </form>

        {!userData ? (
          <div>
            <Button
              variant="contained"
              onClick={() => navigate('/login')}
              fontFamily="Kanit"
              className="nav-navbar-btn"
              style={{ marginRight: '25px' }}
            >
              Log in
            </Button>
            <Button
              variant="contained"
              fontFamily="Kanit"
              onClick={() => navigate('/signup')}
              href={`/signup`}
              className="nav-navbar-btn"
              style={{ marginRight: '15px' }}
            >
              Sign up
            </Button>
          </div>
        ) : (
          <>
            {userData !== null && userData.role === 2 ? (
              <Button
                variant="contained"
                fontFamily="Kanit"
                className="nav-navbar-btn"
                style={{ marginRight: '-300px' }}
                onClick={() => navigate('/users')}
              >
                Users
              </Button>
            ) : null}
            {userData !== null && userData.role === 2 ? (
              <Button
                variant="contained"
                fontFamily="Kanit"
                className="nav-navbar-btn"
                style={{ marginRight: '-300px' }}
                onClick={() => navigate('/addons-approval')}
              >
                Addons Approval
              </Button>
            ) : null}
            <Dropdown>
              <Dropdown.Toggle
                id="dropdown-navbar-btn"
                className="dropdown-btn-navbar"
                variant="secondary"
                align="end"
              >
                <img
                  src={userData.userAvatar}
                  alt="user-avatar"
                  className="dropdown-avatar"
                />{' '}
                <strong className="dropdown-username" id="text-navBar">
                  {userData.firstName}
                </strong>
              </Dropdown.Toggle>
              <Dropdown.Menu variant="dark" align="end">
                <Dropdown.Item
                  onClick={() => navigate('/profile')}
                  id="text-navBar"
                >
                  Profile
                </Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item onClick={userLogOut}>
                  <BoxArrowRight />
                  <span className="ml-5" id="text-navBar">
                    {' '}
                    Log out{' '}
                  </span>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </>
        )}
      </Container>
    </Navbar>
  );
}
