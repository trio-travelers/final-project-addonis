import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import AppContext from "../../Providers/App.Context";
import { loginUser } from "../../Services/auth.services";
import { getUserData } from "../../Services/user.services";
import { Card, Form, Button, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import Stack from "@mui/material/Stack";

const Login = () => {
  const navigate = useNavigate();
  const { setContext } = useContext(AppContext);
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");

  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const updateForm = (prop) => (e) => {
    setUser({
      ...user,
      [prop]: e.target.value,
    });
  };

  async function login(e) {
    e.preventDefault();

    setError("");
    setMessage("");

    await loginUser(user.email, user.password)
      .then(async (u) => {
        const snapshot = await getUserData(u.user.uid);
        if (snapshot.exists()) {
          setContext({
            user: u.user,
            userData: snapshot.val()[Object.keys(snapshot.val())[0]],
          });
          setMessage("Login successful, redirecting you to home page");
          setTimeout(() => {
            navigate("/home");
          }, 3000);
        }
      })
      .catch((err) => {
        if (err.message.includes(`invalid-email`)) {
          setError(`Email ${user.email} is not valid or not registered!`);
        } else if (err.message.includes(`wrong-password`)) {
          setError(`Password is not correct!`);
        } else {
          setError(err.message);
        }
      });
  }

  return (
    <>
      {error && (
        <Stack className="error-popup" sx={{ width: "100%" }} spacing={2}>
          <Alert severity="error" variant="filled" onClose={() => setError("")}>
            <AlertTitle>Error</AlertTitle>
            <strong>{error}</strong>
          </Alert>
        </Stack>
      )}
      {message && (
        <Stack className="message-popup" sx={{ width: "100%" }} spacing={2}>
          <Alert
            severity="success"
            variant="filled"
            onClose={() => setMessage("")}
          >
            <AlertTitle>Success</AlertTitle>
            <strong>{message}</strong>
          </Alert>
        </Stack>
      )}
      <div>
        <Container
          className="d-flex align-items-center justify-content-center"
          style={{ minHeight: "100vh" }}
        >
          <div className="w-100" style={{ maxWidth: "400px" }}>
            <Card style={{ backgroundColor: "rgba(97,121,157,.9)" }}>
              <Card.Body>
                <h2 className="text-center mb-4" style={{ color: "#1f3656" }}>
                  LOG IN
                </h2>
                <Form>
                  <Form.Group id="email">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>Email</Form.Label>
                    <Form.Control
                      type="email"
                      style={{ opacity: "70%" }}
                      value={user.email}
                      onChange={updateForm("email")}
                      required
                    />
                  </Form.Group>
                  <Form.Group id="password">
                    <Form.Label className="mt-2 h6" style={{ color: "#1f3656" }}>Password</Form.Label>
                    <Form.Control
                      type="password"
                      style={{ opacity: "70%" }}
                      value={user.password}
                      onChange={updateForm("password")}
                      required
                    />
                  </Form.Group>
                  <Button
                    className="w-100 mt-4"
                    onClick={login}
                    fontFamily="Kanit"
                    style={{
                      backgroundColor: "#6399c6",
                      mr: "-200px",
                      borderColor: "#1f3656",
                    }}
                    type="submit"
                  >
                    Log In
                  </Button>
                </Form>
                <div className="w-100 text-center mt-3 h6">
                  <Link to="/forgot-password" style={{ color: "#96a8d4" }}>Forgot Password ?</Link>
                </div>
                <div className="w-100 text-center mt-2 h6" style={{ color: "#1f3656" }}>
                  Don't have an account ? <Link to="/signup" style={{ color: "#96a8d4" }}>Sign up</Link>
                </div>
              </Card.Body>
            </Card>
          </div>
        </Container>
      </div>
    </>
  );
};

export default Login;
