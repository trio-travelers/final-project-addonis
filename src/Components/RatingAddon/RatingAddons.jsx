import { Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import { FaStar } from 'react-icons/fa';
import AppContext from '../../Providers/App.Context';
import { updateRating } from '../../Services/addons.services';
import './RatingAddon.css';

export const RatingAddon = ({ addon }) => {
  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState(null);
  const { userData } = useContext(AppContext);

  useEffect(() => {
    setRating(addon.rating);
  }, [addon]);

  const handleRating = (currentRating) => {
    const newRating =
      (addon.rating * addon.timesRated + currentRating) /
      (addon.timesRated + 1);

    setRating(newRating);

    return updateRating(addon.id, newRating, addon.timesRated + 1);
  };
  return (
    <div>
      {[...Array(5)].map((star, i) => {
        const ratingValue = i + 1;
        return (
          <label key={i}>
            {userData.isBlocked ? null : (
              <input
                type="radio"
                name="rating"
                value={ratingValue}
                onClick={() => handleRating(ratingValue)}
              />
            )}

            <FaStar
              className="star"
              color={ratingValue <= (hover || rating) ? '#ffc107' : '#e4e5e9'}
              size="40"
              // size={20}
              onMouseEnter={() => setHover(ratingValue)}
              onMouseLeave={() => setHover(null)}
            />
          </label>
        );
      })}
      <Typography
        variant="body2"
        color="#22528d"
        fontFamily="Kanit"
        textAlign="center"
      >
        Rating {rating ? rating.toFixed(1) : 0} from {addon.timesRated} votes
      </Typography>
      {/* <p>Rating {rating ? rating.toFixed(1) : 0}</p> */}
    </div>
  );
};
