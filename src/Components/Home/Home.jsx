import React from "react";
import FeaturedAddonSlider from "../AddonSlider/FeaturedAddonSlider";
import NewAddonSlider from "../AddonSlider/NewAddonSlider";
import PopularAddonSlider from "../AddonSlider/PopularAddonSlider";
import "./Home.css";

export default function Home() {
  return (
    <div>
      <div className="container">
        <br></br>
        <br></br>
        <h4
          className="text-center font-weight-bold text-dark"
          id="text-category"
        >
          Featured
        </h4>
        <FeaturedAddonSlider />
        <br></br>
        <br></br>
        <br></br>
        <h4
          className="text-center font-weight-bold text-dark"
          id="text-category"
        >
          Popular
        </h4>
        <PopularAddonSlider />
        <br></br>
        <br></br>
        <br></br>
        <h4
          className="text-center font-weight-bold text-dark"
          id="text-category"
        >
          New
        </h4>
        <NewAddonSlider />
        <br></br>
      </div>
    </div>
  );
}
