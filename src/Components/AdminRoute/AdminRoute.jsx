import { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import AppContext from '../../Providers/App.Context';

export default function AdminRoute({ children }) {
  const { userData } = useContext(AppContext);

  if (!userData) return;

  return userData.role === 2 ? children : <Navigate to="/home" />;
}
