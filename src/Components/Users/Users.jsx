import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getAllUsers } from '../../Services/user.services';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import './Users.css';

export default function Users() {
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const [error, setError] = useState('');

  const columns = [
    {
      field: 'userAvatar',
      headerName: 'Avatar',
      width: 80,
      flex: 1,
      renderCell: (params) => {
        return (
          <div>
            <img src={params.row.userAvatar} className="userAvatar" alt="" />
          </div>
        );
      },
    },
    { field: 'userName', headerName: 'Username', width: 130, flex: 1 },
    { field: 'email', headerName: 'Email', width: 170, flex: 1 },
    { field: 'phoneNumber', headerName: 'Phone', width: 150, flex: 1 },
    {
      field: 'role',
      headerName: 'Role',
      width: 100,
      flex: 1,
      renderCell: (params) => {
        return (
          <div>
            <span> {params.row.role === 1 ? 'Basic User' : 'Admin'} </span>
          </div>
        );
      },
    },
    {
      field: 'isBlocked',
      headerName: 'Blocked',
      width: 70,
      flex: 1,
      renderCell: (params) => {
        return (
          <div>
            {params.row.isBlocked ? (
              <DoneIcon color="primary" />
            ) : (
              <ClearIcon color="primary" />
            )}
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    getAllUsers()
      .then((snapshot) => {
        setUsers(Object.values(snapshot.val()));
      })
      .catch((err) => setError(err.message));
  }, []);

  const navigateToProfile = (userName) => {
    return navigate(`/users/${userName}`);
  };

  return (
    <>
      {error && (
        <Stack className="error-popup" sx={{ width: '100%' }} spacing={2}>
          <Alert severity="error" variant="filled" onClose={() => setError('')}>
            <AlertTitle>Error</AlertTitle>
            <strong>{error}</strong>
          </Alert>
        </Stack>
      )}
      <div
        style={{
          height: '450px',
          maxWidth: '75%',
          margin: '2em auto',
          backgroundColor: '#acb4db',
        }}
      >
        <DataGrid
          onCellClick={(params) => navigateToProfile(params.row.userName)}
          pageSize={5}
          rowsPerPageOptions={[5]}
          rows={users}
          columns={columns}
          getRowId={(u) => u.uid}
          components={{ Toolbar: GridToolbar }}
        />
      </div>
    </>
  );
}
