import React, { useState } from 'react';
import { Card, Form, Button, Alert, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { resetPassword } from '../../Services/auth.services';

export default function ForgotPassword() {
  const [error, setError] = useState('');
  const [message, setMessage] = useState('');
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState({
    email: '',
    password: '',
    confirmPassword: '',
  });

  const updateForm = (prop) => (e) => {
    setUser({
      ...user,
      [prop]: e.target.value,
    });
  };
  async function handleSubmit(e) {
    e.preventDefault();

    try {
      setMessage('');
      setError('');
      setLoading(true);
      await resetPassword(user.email);
      setMessage('Check your inbox for further instructions');
    } catch (error) {
      setError(`${error.message}`);
    }

    setLoading(false);
  }

  return (
    <Container
      className="d-flex align-items-center justify-content-center"
      style={{ minHeight: '100vh' }}
    >
      <div
        className="w-100"
        style={{ maxWidth: '400px', backgroundColor: 'rgba(97,121,157,.9)' }}
      >
        <Card style={{ backgroundColor: 'rgba(97,121,157,.9)' }}>
          <Card.Body>
            <h2 className="text-center mb-4">Password Reset</h2>
            {error && <Alert variant="danger">{error}</Alert>}
            {message && <Alert variant="success">{message}</Alert>}
            <Form onSubmit={handleSubmit}>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  value={user.email}
                  onChange={updateForm('email')}
                  required
                />
              </Form.Group>

              <Button disabled={loading} className="w-100 mt-4" type="submit">
                Reset Password
              </Button>
            </Form>
            <div className="w-100 text-center mt-3">
              <Link to="/login">Login</Link>
            </div>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2">
          Haven't signed up yet ? <Link to="/signup">Sign Up</Link>
        </div>
      </div>
    </Container>
  );
}
