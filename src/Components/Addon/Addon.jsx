const Addon = ({addon}) => {

    return (
        <div className='Tweet'>
            <div>{addon.name}</div> <br></br>
            <div>{addon.ide}</div> <br></br>
            <div>{addon.description}</div> <br></br>
            <div>{addon.tags}</div> <br></br>
            <div>{addon.origin}</div> <br></br>
            <div>{addon.author}</div> <br></br>
            <div>------------------------------------</div>
            {/* <div className='Tweet-Meta'><span>Likes: {tweet.likedBy.length}</span> */}
            {/* <button onClick={() => like(tweet)} disabled={tweet.likedBy.includes(handle)}>Like</button> */}
            {/* </div> */}
        </div>
    )
}

export default Addon;