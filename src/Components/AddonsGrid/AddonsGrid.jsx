import React, { useEffect, useState } from "react";
import { getAllAddons } from "../../Services/addons.services";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import AddonCard from "../AddonCard/AddonCard";
import { useSearchParams } from "react-router-dom";

const AddonsByTag = () => {
  const [addons, setAddons] = useState();
  const [searchParam] = useSearchParams();
  const searchQuery = searchParam.get("search");
  const searchTag = searchParam.get("tag");
  const searchIDE = searchParam.get("ide");

  useEffect(() => {
    if (searchTag) {
      getAllAddons().then((res) => {
        const filtered = res.filter((el) =>
          el.tags.toLowerCase().includes(searchTag.toLowerCase())
        );
        setAddons(filtered);
      });
    }
  }, [searchTag]);

  useEffect(() => {
    if (searchIDE) {
      getAllAddons().then((res) => {
        const filtered = res.filter((el) =>
          el.ide.toLowerCase().includes(searchIDE.toLowerCase())
        );
        setAddons(filtered);
      });
    }
  }, [searchIDE]);

  useEffect(() => {
    if (searchQuery) {
      getAllAddons().then((res) => {
        const filtered = res.filter(
          (el) =>
            el.name.toLowerCase().includes(searchQuery.toLowerCase()) ||
            el.tags.toLowerCase().includes(searchQuery.toLowerCase()) ||
            el.ide.toLowerCase().includes(searchQuery.toLowerCase())
        );
        setAddons(filtered);
      });
    }
  }, [searchQuery]);

  useEffect(() => {
    if (
      (!searchQuery || searchQuery === "undefined") &&
      !searchIDE &&
      !searchTag
    ) {
      getAllAddons().then((res) => {
        setAddons(res);
      });
    }
  }, []);

  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        width: "75%",
        margin: "1em auto",
        flexDirection: "row",
      }}
    >
      {!addons ? (
        <h1> Loading ....</h1>
      ) : (
        addons &&
        addons
          .filter((e) => e.pending === false)
          .map((addon, index) => {
            return (
              <AddonCard
                key={index}
                ImageUrl={addon.ImageUrl}
                name={addon.name}
                author={addon.author}
                ide={addon.ide}
                rating={+addon.rating}
              />
            );
          })
      )}
      <div style={{height: "500px"}}></div>
    </div>
  );
};
export default AddonsByTag;
