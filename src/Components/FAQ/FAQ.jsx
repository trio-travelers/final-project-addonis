import React from 'react';

export default function FAQ() {
  return (
    <div style={{ margin: '60px' }}>
      <section>
        <h3 className="text-center mb-4 pb-2 text-dark fw-bold">FAQ</h3>
        <p className="text-center mb-5 ">
          Find the answers for the most frequently asked questions below
        </p>

        <div className="row">
          <div className="col-md-6 col-lg-4 mb-4">
            <h4 className="mb-3 text-dark fw-bold">
              <i className="far fa-paper-plane pe-2"></i> How can I upload
              addons?
            </h4>
            <p>
              To upload addons you must be a registered user, after that you can
              navigate to the upload addon form through the upload addon button
              in the navigation menu on top. Fill out the fields and upload your
              files. If everything goes correctly, you will have your first
              uploaded addon
            </p>
          </div>

          <div className="col-md-6 col-lg-4 mb-4">
            <h4 className="mb-3 text-dark fw-bold">
              <i className="fas fa-pen-alt text-secondary pe-2"></i> Can I
              download addons ?
            </h4>
            <strong>
              <u>Yes, it is possible!</u>
            </strong>{' '}
            <p>
              All you need to do is select the addon you wish to download,
              clicking on the addon card will redirect you to a page which
              contains additional information about the addon. The download
              button is on top Psst: You'cant miss it, its called{' '}
              <strong>
                <u> Download </u>
              </strong>
            </p>
          </div>

          <div className="col-md-6 col-lg-4 mb-4">
            <h4 className="mb-3 text-dark fw-bold">
              <i className="fas fa-user text-secondary pe-2"></i> Can I see a
              list of all the addons ?
            </h4>
            <strong>
              <u> Yes, you can!</u>
            </strong>
            <p>
              You can see a list of all the addons by clicking the Addons button
              in the navigation menu, you can access that without needing to be
              logged in or registered. You have options to filter, sort and
              navigate to the detailed page of every addon.
            </p>
          </div>

          <div className="col-md-6 col-lg-4 mb-4">
            <h4 className="mb-3 text-dark fw-bold">
              <i className="fas fa-rocket text-secondary pe-2"></i> I uploaded
              an addon but I don't see it ?
            </h4>
            <p>
              All addons uploaded by our users must go through admin
              verification, once your addon is verified it will appear in your
              profile and on the home page as well.
            </p>
          </div>

          <div className="col-md-6 col-lg-4 mb-4">
            <h4 className="mb-3 text-dark fw-bold">
              <i className="fas fa-home text-secondary pe-2"></i> I can't upload
              addons but I am logged in ?
            </h4>
            <strong>
              <u>You were most likely blocked</u>.
            </strong>{' '}
            <p>
              Unfortunately some of our users may be a bit eager to upload
              addons quite often, some of which don't belong to our site, so if
              an admin decided to block you, you must have broken the rules.
              While blocked you cannot upload addons or rate addons.
              <strong>You can get unblocked, it's not permanent</strong>
            </p>
          </div>

          <div className="col-md-6 col-lg-4 mb-4">
            <h4 className="mb-3 text-dark fw-bold">
              <i className="fas fa-book-open text-secondary pe-2"></i> Can I see
              the profile of other users ?
            </h4>
            <p>
              Yes you can, clicking the name of an addon author or other users
              will redirect you to their profile where you can see more details
              about them and the addons they have uploaded.
            </p>
          </div>
        </div>
      </section>
    </div>
  );
}
