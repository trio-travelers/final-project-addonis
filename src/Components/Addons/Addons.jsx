import { useEffect, useState } from 'react';
import {
  addAddon,
  checkAddonName,
  getAddonById,
  getAddonByName,
} from '../../Services/addons.services';
import Addon from '../Addon/Addon';
import AppContext from '../../Providers/App.Context';
import { useContext } from 'react';
import { Button, Form } from 'react-bootstrap';
import { storage } from '../../Config/Firebase-config';
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from 'firebase/storage';
import { updateAddonUrl } from '../../Services/addons.services';
import { updateAddonImageUrl } from '../../Services/addons.services';
import { ref, uploadBytesResumable } from '@firebase/storage';
import './Addons.css';

const Addons = () => {
  const { userData, setContext, user } = useContext(AppContext);
  const [error, setError] = useState('');
  const [filePath, setFilePath] = useState('');
  const [imagePath, setImagePath] = useState('');
  const [progress, setProgress] = useState(0);

  const [addon, setAddon] = useState({
    name: '',
    ide: '',
    description: '',
    repoOwner: '',
    repoName: '',
    tags: '',
    author: '',
    categories: '',
    pending: true,
    rating: 0,
    timesRated: 0,
    downloads: 0,
    downloadUrl: '',
    status: '',
    fileName: '',
    imageName: '',
  });

  const [addons, setAddons] = useState([]);

  const updateForm = (prop) => (e) => {
    setAddon({
      ...addon,
      [prop]: e.target.value,
      author: userData.userName,
      timesRated: 0,
    });
  };

  const updateUploadFile = (prop) => (e) => {
    e.preventDefault();
    setFilePath(e.target.files[0]);
    setAddon({
      ...addon,
      [prop]: e.target.value,
      fileName: e.target.files[0].name,
    });
  };

  const updateUploadPicture = (prop) => (e) => {
    e.preventDefault();
    setImagePath(e.target.files[0]);
    setAddon({
      ...addon,
      [prop]: e.target.value,
      imageName: e.target.files[0].name,
    });
  };

  const uploadAddonImage = (uid) => {
    const imageFile = imagePath;

    if (!imageFile) return alert(`Please select a file for upload!`);

    const uploadedFile = storageRef(
      storage,
      `addons/images/${uid}/${imageFile.name}`
    );

    uploadBytes(uploadedFile, imageFile)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateAddonImageUrl(uid, url);
        });
      })
      .catch(console.error);
  };

  const createAddon = (content) => async (e) => {
    e.preventDefault();

    if (addon.name.length < 2 || addon.name.length > 30) {
      alert('User name must be between 2 and 20 symbols');
      return;
    }
    if (addon.tags.length < 2) {
      alert('Your Addon must have at least one tag');
      return;
    }
    if (addon.ide.includes(',') || addon.ide.includes(';')) {
      alert('Your Addon must have only one IDE');
      return;
    }
    if (addon.description.length <= 15) {
      alert('The Addon description cannot be shorter than 15 symbols');
      return;
    }

    try {
      const addon = await addAddon(content);
      setAddons([addon, ...addons]);

      uploadAddon(addon.uid);
      uploadAddonImage(addon.uid);

      return addon;
    } catch (error) {
      setError(`${error.message}`);
    }
  };

  const uploadAddon = (uid) => {
    const file = filePath;

    if (!file) return alert(`Please select a file for upload!`);

    const uploadedFile = storageRef(
      storage,
      `addons/programfiles/${uid}/${file.name}`
    );

    //---------------------------------------
    const uploadTask = uploadBytesResumable(uploadedFile, file);
    uploadTask.on(
      'state_changed',
      (snapshot) => {
        const prog = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(prog);
      },
      (err) => console.log(err),
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then(
          (url) => (addon.downloadUrl = url)
        );
      }
    );
    //---------------------------------------
    uploadBytes(uploadedFile, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateAddonUrl(uid, url);
        });
      })
      .catch(console.error);
  };
  if (!userData) return;
  return (
    <>
      <div className="containerUpload">
        {userData.isBlocked ? (
          <h1
            style={{
              color: 'red',
              marginBottom: '385px',
              marginLeft: '-20px',
              width: '550px',
            }}
          >
            <strong>
              <u>You are blocked.</u>
            </strong>
            <p style={{ marginLeft: '-40px' }}>Cannot upload addons. </p>
          </h1>
        ) : (
          <form>
            <div>Addon Name</div>
            <input onChange={updateForm('name')} style={{ width: '500px' }} />
            <div style={{ marginTop: '10px' }}>IDE</div>
            <input onChange={updateForm('ide')} style={{ width: '500px' }} />
            <div style={{ marginTop: '10px' }}>Description</div>
            <input
              onChange={updateForm('description')}
              style={{ width: '500px', height: '100px' }}
            />
            <div style={{ marginTop: '10px' }}>Repository Owner</div>
            <input
              onChange={updateForm('repoOwner')}
              style={{ width: '500px' }}
            />
            <div style={{ marginTop: '10px' }}>Repository Name</div>
            <input
              onChange={updateForm('repoName')}
              style={{ width: '500px' }}
            />
            <div style={{ marginTop: '10px' }}>Tags</div>
            <input onChange={updateForm('tags')} style={{ width: '500px' }} />
            <div style={{ marginTop: '10px' }}>Choose Addon File</div>
            <input
              type="file"
              name="file"
              id="file"
              className="choose-file"
              onChange={updateUploadFile()}
            />{' '}
            <br></br>
            <div style={{ marginTop: '10px' }}>Choose Addon Banner</div>
            <input
              type="file"
              name="file"
              id="file"
              className="choose-file"
              onChange={updateUploadPicture()}
            />
            <br></br>
            <br></br>
            <p>
              <button onClick={createAddon(addon)}>Submit</button>
            </p>
            <h5>Uploading {progress} %</h5>
          </form>
        )}
      </div>
    </>
  );
};

export default Addons;
