import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardActionArea from "@mui/material/CardActionArea";
import Typography from "@mui/material/Typography";
import Tooltip from "@mui/material/Tooltip";
import "./AddonCard.css";
import PropTypes from "prop-types";
import { Rating } from "@mui/material";
import StarIcon from "@mui/icons-material/Star";

const AddonCard = ({ name, author, ide, ImageUrl, rating }) => {
  return (
    <Card
      sx={{
        width: 200,
        borderRadius: 3,
        border: 2,
        borderColor: "#1f3656",
        height: 200,
        // display: "flex",
        alignContent: "space-between",
        boxShadow: 4,
        backgroundColor: "#dee1f1",
        margin: "1em",
      }}
    >
      <CardActionArea href={`/addon/${name}`}>
        <CardMedia
          component="img"
          src={ImageUrl}
          alt={name}
          height="110"
          sx={{
            maxWidth: 120,
            marginLeft: "auto",
            marginRight: "auto",
            objectFit: "contain",
          }}
        />

        <CardContent>
          <Tooltip title={name}>
            <Typography
              variant="body1"
              color="#22528d"
              fontFamily="Kanit"
              textAlign="center"
            >
              {name}
            </Typography>
          </Tooltip>

          <Grid item xs={6} sx={{ wordWrap: "break-word" }}>
            <Typography
              variant="body2"
              color="hsl(266, 53%, 57%)"
              fontFamily="Kanit"
              textAlign="center"
            >
              {ide}
            </Typography>
          </Grid>
          <Rating
            name="text-feedback"
            value={rating}
            readOnly
            size="small"
            precision={0.5}
            emptyIcon={
              <StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />
            }
          />
          {/* </Box> */}
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

AddonCard.propTypes = {
  name: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  ide: PropTypes.string.isRequired,
  ImageUrl: PropTypes.string.isRequired,
};

export default AddonCard;
