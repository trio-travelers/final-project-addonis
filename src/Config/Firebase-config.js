import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth';
import {getDatabase} from 'firebase/database';
import {getStorage} from 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyBE-_dORVLk8f_Wa-3i8j81k9seeTW7S9g",
  authDomain: "final-project-adonis.firebaseapp.com",
  projectId: "final-project-adonis",
  storageBucket: "gs://final-project-adonis.appspot.com",
  messagingSenderId: "886156802741",
  appId: "1:886156802741:web:a3885c331fe1ea04241cfc",
  databaseURL: "https://final-project-adonis-default-rtdb.europe-west1.firebasedatabase.app",
};



// Initialize Firebase
export const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);