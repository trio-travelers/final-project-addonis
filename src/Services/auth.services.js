import { auth } from "../Config/Firebase-config";
import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, updateEmail, sendPasswordResetEmail } from "firebase/auth";


export const registerUser = (email, password) => {

    return createUserWithEmailAndPassword(auth, email, password);
}


export const loginUser = (email, password) => {

    return signInWithEmailAndPassword(auth, email, password);
}

export const logoutUser = () => {
    return signOut(auth);
}

export const updateUserEmail = (user, email) => {
    return updateEmail(user, email)
}

export const resetPassword = (email) => {
    return sendPasswordResetEmail(auth, email)
}
