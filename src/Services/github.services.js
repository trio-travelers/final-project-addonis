import axios from "axios";

export const githubApiUrl = "https://api.github.com/repos/";

export const getRepoInfo = (link) => {
  return fetch(`https://api.github.com/repos/${link}`).then((data) =>
    data.json()
  );
};

export const pullsCount = (link) => {
  return axios
    .get(githubApiUrl + link + "/pulls")
    .then((result) => {
      return result.data.length;
    })
    .catch((err) => console.log(err));
};

export const getCommitInfo = (link) => {
  return axios
    .get(githubApiUrl + link + "/commits")
    .then((result) => {
      const commitDate = result.data[0].commit.author.date;
      const date = commitDate;
      return date;
    })
    .catch((err) => console.log(err));
};

export const issuesCount = (link) => {
  return axios
    .get(githubApiUrl + link + "/issues")
    .then((result) => {
      return result.data.length;
    })
    .catch((err) => console.log(err));
};
