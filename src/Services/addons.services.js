import {
  ref,
  push,
  get,
  query,
  equalTo,
  orderByChild,
  update,
  runTransaction,
} from "firebase/database";
import { db } from "../Config/Firebase-config";
import { storage } from "../Config/Firebase-config";
import { ref as storageRef, deleteObject } from "firebase/storage";

export const addAddon = (addon) => {
  return push(ref(db, "addons"), { ...addon, date: new Date().valueOf() }).then(
    (result) => {
      return getAddonById(result.key);
    }
  );
};

export const getAddonById = (id) => {
  return get(ref(db, `addons/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Addon with id ${id} does not exist!`);
    }

    const addon = result.val();
    addon.uid = id;

    return addon;
  });
};

export const updateAddonUrl = (addonUid, url) => {
  return update(ref(db), {
    [`addons/${addonUid}/downloadUrl`]: url,
  });
};

export const updateAddonImageUrl = (addonUid, url) => {
  return update(ref(db), {
    [`addons/${addonUid}/ImageUrl`]: url,
  });
};

const fromAddonsDocument = (snapshot) => {
  const addonsDocument = snapshot.val();

  return Object.keys(addonsDocument).map((key) => {
    const addon = addonsDocument[key];

    return {
      ...addon,
      id: key,
      ratedBy: addon.ratedBy ? Object.keys(addon.ratedBy) : [],
    };
  });
};

export const getAllAddons = () => {
  return get(ref(db, "addons")).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromAddonsDocument(snapshot);
  });
};

export const getAddonByName = (name) => {
  return get(
    query(ref(db, "addons"), orderByChild("name"), equalTo(name))
  ).then((snapshot) => {
    if (!snapshot.exists()) {
      return;
    }
    return fromAddonsDocument(snapshot)[0];
  });
};

export const updateDownloads = (addonUid, downloads) => {
  return update(ref(db), {
    [`addons/${addonUid}/downloads`]: downloads,
  });
};

//--------------------- RATING

export const updateRating = (id, newRating, timesRated) => {
  return update(ref(db), {
    [`addons/${id}/rating`]: newRating,
    [`addons/${id}/timesRated`]: timesRated,
  });
};

export const rateAddon = (username, addonId, newRating, timesRated) => {
  const updateRating = {};
  updateRating[`/addons/${addonId}/ratedBy/${username}`] = true;
  updateRating[`/users/${username}/ratedAddons/${addonId}`] = true;

  return update(ref(db), updateRating).then(() => {
    return runTransaction(ref(db, `addons/${addonId}`), (addon) => {
      addon.rating = newRating;
      addon.timesRated = timesRated++;

      return addon;
    });
  });
};

//--------------------- APPROVED/REJECTED STATUS

export const updateStatus = (uid, status) => {
  return update(ref(db), {
    [`addons/${uid}/status`]: status,
  });
};

//--------------------- DELETE ADDON

export const deleteAddon = async (id, file, image) => {
  const addon = await getAddonById(id);
  const addonFile = storageRef(storage, `addons/images/${id}/${image}`);
  const addonLogo = storageRef(storage, `addons/programfiles/${id}/${file}`);

  await update(ref(db), {
    [`addons/${id}`]: null,
  });

  await deleteObject(addonFile);
  await deleteObject(addonLogo);
};

export const approveAddon = (addonId) => {
  return update(ref(db), {
    [`addons/${addonId}/pending`]: false,
  });
};

export const updateAddonTitle = (id, title) => {
  return update(ref(db), {
    [`addons/${id}/name`]: title,
  });
};

export const updateAddonDescription = (id, description) => {
  return update(ref(db), {
    [`addons/${id}/description`]: description,
  });
};

export const updateAddonTags = (id, tags) => {
  return update(ref(db), {
    [`addons/${id}/tags`]: tags,
  });
};

export const updateAddonIde = (id, ide) => {
  return update(ref(db), {
    [`addons/${id}/ide`]: ide,
  });
};

export const updateAddonPicture = (id, url) => {
  return update(ref(db), {
    [`addons/images/${id}`]: url,
  });
};

export const getAddonsByAuthor = async (username) => {
  const snapshot = await get(
    query(ref(db, "addons"), orderByChild("author"), equalTo(username))
  );
  if (!snapshot.exists()) return [];
  return fromAddonsDocument(snapshot);
};
