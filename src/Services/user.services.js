import { get, set, ref, query, equalTo, orderByChild, update } from 'firebase/database';
import { userRole } from '../common/user-role';
import { db } from '../Config/Firebase-config';

export const getUserByUsername = (userName) => {

  return get(ref(db, `users/${userName}`))
};

export const getUserData = (uid) => {

  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)))
};

export const additionalUserInfo = (userName, uid, email, phoneNumber, firstName, lastName) => {

  return set(ref(db, `users/${userName}`), { userName, uid, email, phoneNumber, firstName, lastName, ratedAddons: {}, isBlocked: false, role: userRole.BASIC, userAvatar: 'https://pic.onlinewebfonts.com/svg/img_568656.png' });
};

export const updateUserRole = (username, role) => {
  return update(ref(db), {
    [`users/${username}/role`]: role
  })
}

export const getAllUsers = () => {
  return get(ref(db, 'users/'))
}

export const blockUser = (username) => {
  return update(ref(db), {
    [`users/${username}/isBlocked`]: true
  })
}

export const unblockUser = (username) => {
  return update(ref(db), {
    [`users/${username}/isBlocked`]: false
  })
}

export const updateUserProfilePicture = (username, url) => {
  return update(ref(db), {
    [`users/${username}/userAvatar`]: url,
  });
};

export const updateDBUserEmail = (username, email) => {
  return update(ref(db), {
    [`users/${username}/email`]: email,
  });
};

export const updateUserFirstName = (username, firstName) => {
  return update(ref(db), {
    [`users/${username}/firstName`]: firstName,
  })
}
export const updateUserLastName = (username, lastName) => {
  return update(ref(db), {
    [`users/${username}/lastName`]: lastName,
  })
}
