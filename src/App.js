import React, { useEffect, useState } from 'react';
import AppContext from './Providers/App.Context';
import { getUserData } from './Services/user.services';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './Config/Firebase-config';
import SignUp from './Components/SignUp/SignUp';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './Components/Login/Login';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './Components/Home/Home';
import NavBar from './Components/NavBar/NavBar';
import Profile from './Components/Profile/Profile';
import PrivateRoute from './Components/PrivateRoute/PrivateRoute'
import UserProfile from './Components/UserProfile/UserProfile';
import Addons from './Components/Addons/Addons';
import UpdateProfile from './Components/UpdateProfile/UpdateProfile';
import Users from './Components/Users/Users';
import AddonItem from './Components/AddonItem/AddonItem';
import { QueryClient, QueryClientProvider } from 'react-query';
import AdminRoute from './Components/AdminRoute/AdminRoute';
import AddonsList from './Components/AddonsList/AddonsList';
import AddonsApproval from './Components/AddonApprove/AddonsApproval';
import AddonsByTag from './Components/AddonsGrid/AddonsGrid';
import { UpdateAddon } from './Components/UpdateAddon/UpdateAddon';
import Footer from './Components/Footer/Footer';
import AboutUs from './Components/AboutUs/AboutUs';
import TermsAndConditions from './Components/TermsAndConditions/TermsAndConditions';
import FAQ from './Components/FAQ/FAQ';
import PageNotFound from './Components/PageNotFound/PageNotFound';
import ForgotPassword from './Components/ForgotPassword/ForgotPassword';

function App() {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  })

  const [error, setError] = useState('');
  let [user, loading] = useAuthState(auth);

  const queryClient = new QueryClient();

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists) {
          return setError('User does not exist :(');
        }

        setAppState({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]]
        })
      })
      .catch(err => setError(`${err.message}`))

  }, [user]);


  return (
    <Router>
      <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
        <QueryClientProvider client={queryClient}>

          < NavBar />

          {!loading &&
            <div style={{ height: 450, width: '100%', margin: '0 auto' }}>

              <Routes>
                <Route path='/profile' element={
                  <PrivateRoute user={user}>
                    <Profile />
                  </PrivateRoute>} />

                <Route path='/update-profile' element={
                  <PrivateRoute user={user}>
                    <UpdateProfile />
                  </PrivateRoute>} />


                <Route path='/users' element={
                  <AdminRoute>
                    <Users />
                  </AdminRoute>
                } />

                <Route path='addons-approval' element={
                  <AdminRoute>
                    <AddonsApproval />
                  </AdminRoute>
                } />

                <Route path="/upload" element={
                  <PrivateRoute user={user}>
                    <Addons />
                  </PrivateRoute>
                } />

                <Route path='/forgot-password' element={<ForgotPassword />} />
                <Route path='/addons-list' element={<AddonsList />} />
                <Route path='users/:username' element={< UserProfile />} />
                <Route path="/signup" element={<SignUp />} />
                <Route path="/login" element={<Login />} />
                <Route path="/home" element={<Home />} />
                <Route path="/" element={<Home />} />

                <Route path="/addons" element={<AddonsByTag />} />
                <Route path="/addon/:name" element={<AddonItem />} />
                <Route path="/addons/:author/:name" element={
                  <PrivateRoute user={user}>
                    <UpdateAddon />
                  </PrivateRoute>} />
                <Route path='/about-us' element={<AboutUs />} />
                <Route path='/terms-and-conditions' element={<TermsAndConditions />} />
                <Route path='/FAQ' element={<FAQ />} />
                <Route path="*" element={< PageNotFound />} />
              </Routes>
              <Footer />
            </div>
          }
        </QueryClientProvider>
      </AppContext.Provider>
    </Router>
  );
}

export default App;
