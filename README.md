## FINAL PROJECT ADDONIS
## **The Addon Buddies**
#### A SINGLE PAGE REACT BASED APPLICATION FOR ADDON EXCHANGE 

[![GitHub Official Project](https://scontent.fsof10-1.fna.fbcdn.net/v/t39.30808-6/288621627_10221948830573260_8054405261847945069_n.jpg?stp=dst-jpg_s640x640&_nc_cat=106&ccb=1-7&_nc_sid=730e14&_nc_ohc=ZvFdmMfX4MYAX8gjC3e&_nc_ht=scontent.fsof10-1.fna&oh=00_AT_NRB_iOTSX_7nNdWrD35rh-sxG0yxqbuWcGv8KIPFxRA&oe=62AF54D3)](https://gitlab.com/trio-travelers/final-project-addonis)




## Installation & Usage

* The official GitHub repository of the application can be accessed: **[here](https://gitlab.com/trio-travelers/final-project-addonis)**
* The application is developed via **[Visual Studio Code](https://code.visualstudio.com/)** and can be run & manipulated by it. 

### Installation


Run the following command in Git Bash/Visual Studio Code console:


```ruby
npm install
```

### Start

Run the following command in Git Bash/Visual Studio Code console:


```ruby
npm start
```

## About The Application

This application was developed as a Final Project Task for the **2022 JavaScript & React** course in **[Telerik Academy](lerikacademy.com)**. The project is created by: 

### * **[Mario Vangelov](https://gitlab.com/Swayy9)** * **[Asya Igova](https://gitlab.com/igova)** * **[Veselin Shterev](https://gitlab.com/VeselinShterev)**

<br />

## Used Technologies, Libraries and Realtime Database

#### * **[React](https://reactjs.org/)**
#### * **[Bootstrap](https://getbootstrap.com/)**
#### * **[Material UI](https://mui.com/)**
#### * **[Firebase](https://firebase.google.com/)**


## Features
<br/>

### Basic Features
<br/>

|                        | Accessible to every user |  | Built in validations | |
|------------------------|---|---|---|---|
| User Registration       | + |  | + |  |
| Admin Role              | - |  | + |  |
| Edit Own Profile          | + |  | + |  |
| Upload Own Addon              | + |  | +|  
| Rate Addons               | + |  | + |  |
| Edit Own Addon      | + |  | + |  |
| Delete Own Addon      | + |  | n/a |  |
| Download Other User`s Addons | + |  | n/a 
| Search & Filter Addons | + |  | n/a |  |
| Addon Tags | + |  | + | 
| Addon Link to the original repository Tags | + |  | n/a | 
| Extracted Additional Addon Information From The Original Repository | + |  | n/a |
| Addon Number Of Downloads Count | + |  | n/a |  
| Addon Tags | + |  | + | 

### Additional Admin User Features
<br/>

|                        |  |  |  | |
|------------------------|---|---|---|---|
| Block/Unblock Users       | + |  |  |  |
| Approve/Decline New Addons              | + |  | |  |
| See All Users        | + |  |  |  |
| Search And Filter All Users       | + |  |  |  |



## Gallery

### Homepage
![Homepage](./src/ScreenShots/Homepage.JPG)

### Single Addon View
![Homepage](./src/ScreenShots/SingleAddonView.JPG)

### Edit Addon
![Homepage](./src/ScreenShots/EditAddon.JPG)

### Add Addon
![Homepage](./src/ScreenShots/AddAddon.JPG)

### Addons List
![Homepage](./src/ScreenShots/AddonsList.JPG)

### Users List
![Homepage](./src/ScreenShots/UsersList.JPG)

### Profile
![Homepage](./src/ScreenShots/Profile.JPG)

### Edit Profile
![Homepage](./src/ScreenShots/EditProfile.JPG)